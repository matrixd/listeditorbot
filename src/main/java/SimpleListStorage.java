import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by m0x35 on 4/20/17.
 */
public class SimpleListStorage implements ListStorage {
    public SimpleListStorage() {
        mStorage = new HashMap<>();
    }

    @Override
    public void store(String chatId, List list) {
        mStorage.put(chatId, list);
    }

    @Override
    public Optional<List> get(String chatId) {
        List lst = mStorage.get(chatId);
        if (lst != null) {
            return Optional.of(lst);
        }
        return Optional.empty();
    }

    @Override
    public void remove(String chatId) {
        mStorage.remove(chatId);
    }

    private final Map<String, List> mStorage;
}
