import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import org.bson.BsonDateTime;
import org.bson.Document;

import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by m0x35 on 4/21/17.
 */
public class MongoSessionStorage implements ActionSessionStorage {
    MongoSessionStorage() {
        mCollection = new MongoClient().getDatabase("listEditorBot").getCollection("sessions");
    }

    @Override
    public void store(ActionSession sess) {
        Document doc = new Document("_id", sess.getId())
                .append("user", sess.userId())
                .append("data", sess.getUserInput())
                .append("created", new BsonDateTime(System.currentTimeMillis()));

        mCollection.replaceOne(eq("_id", sess.getId()), doc, new UpdateOptions().upsert(true));
    }

    @Override
    public void remove(ActionSession sess) {
        mCollection.deleteOne(eq("_id", sess.getId()));
    }

    @Override
    public Optional<ActionSession> get(String id) {
        Document doc = mCollection.find(eq("_id", id)).first();

        if (doc == null || doc.isEmpty())
            return Optional.empty();

        return Optional.of(new ActionSession(doc.getString("_id"), doc.getLong("user"), doc.getString("data")));
    }

    private final MongoCollection<Document> mCollection;
}
