import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 * Created by m0x35 on 3/25/17.
 */
public class Main {
    public static void main(String[] args) {

        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new ListEditorBot(new MongoSessionStorage(), new MongoListStorage()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
