/**
 * Created by m0x35 on 4/20/17.
 */
class Help {
    static String getInlineHelp() {
        final String helpString = "Hi, I'm @" + botname + "!\nIn private conversations just mention my nick and enter:\n" +
                "to rename a list - new header\n" +
                "to add new items - new items\n" +
                "to remove an items - indexes of an items\n" +
                "to replae an item - index of an item and text (1;new text)\n" +
                "4) nothing if you want to see this message or delete a list.";

        return helpString;
    }

    static String getHelloWord() {
        final String sayHello = "Hi, I'm @" + botname + "!\n" +
                "Use me to create and edit lists.\n" +
                "I can modify or create only one list per chat instance.\n" +
                "I can be used in private conversations in _inline mode_, or in _command mode_. (enter /helpInline or /helpCmd to read more)\n" +
                "I'm written on Java, sources will be avaible soon.\n" +
                "_Privacy disclaimer_ content of all active lists is stored on the server";

        return sayHello;
    }

    static String getCmdHelp() {
        final String cmdHelp = "_Cooming soon..._";
        return cmdHelp;
    }

    static String botname = "ListEditorBot";
}
