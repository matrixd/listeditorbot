import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by m0x35 on 4/20/17.
 */
public class SimpleActionSessionStorage implements ActionSessionStorage{
    public SimpleActionSessionStorage() {
        mMap = new HashMap<>();
    }

    @Override
    public void store(ActionSession sess) {
        mMap.put(sess.getId(), sess);
    }

    @Override
    public void remove(ActionSession sess) {
        mMap.remove(sess.getId());
    }

    @Override
    public Optional<ActionSession> get(String id) {
        ActionSession sess = mMap.get(id);
        if (sess != null)
            return Optional.of(sess);
        return Optional.empty();
    }

    private final Map<String, ActionSession> mMap;
}
