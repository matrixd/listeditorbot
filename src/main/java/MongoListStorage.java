import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.BsonDateTime;
import org.bson.BsonString;
import org.bson.Document;
import org.bson.types.ObjectId;
import java.util.Optional;

import static com.mongodb.client.model.Filters.*;

/**
 * Created by m0x35 on 4/20/17.
 */
public class MongoListStorage implements ListStorage {

    MongoListStorage() {
        mClient = new MongoClient();
        mDb = mClient.getDatabase("listEditorBot");
        mCollection = mDb.getCollection("lists");
    }

    @Override
    public void store(String chatId, List list) {
        Document doc = new Document("_id", chatId)
                .append("list", list.toBsonDoc())
                .append("lastAccess", new BsonDateTime(System.currentTimeMillis()));

        mCollection.replaceOne(eq("_id", chatId), doc, new UpdateOptions().upsert(true));
    }

    @Override
    public Optional<List> get(String chatId) {
        List lst = null;
        Document doc = mCollection.find(eq("_id", chatId)).first();

        if (doc == null || doc.isEmpty())
            return Optional.empty();

        doc = (Document)doc.get("list");

        if (doc == null || doc.isEmpty())
            return Optional.empty();

        return Optional.of(List.fromBsonDoc(doc));
    }

    @Override
    public void remove(String chatId) {
        mCollection.deleteOne(eq("_id", chatId));
    }

    private final MongoClient mClient;
    private final MongoDatabase mDb;
    private final MongoCollection<Document> mCollection;
}
