/**
 * Created by m0x35 on 4/20/17.
 */
public class BotActions {
    enum Action {
        Add,
        Remove,
        CreateNewList,
        RemoveList,
        RenameList,
        ReplaceItem,
        SortAscending,
        SortDescending,
        Help,
        HelpInline,
        HelpCmd,
        Cancel
    }

    static String getKeywoard(Action action) {
        switch (action) {
            case Add:
                return "add";
            case Remove:
                return "rm";
            case CreateNewList:
                return "crs";
            case RemoveList:
                return "rml";
            case RenameList:
                return "rnl";
            case ReplaceItem:
                return "rpi";
            case SortAscending:
                return "sa";
            case SortDescending:
                return "sd";
            case HelpInline:
                return "hlp";
            case Cancel:
                return "cnl";
        }

        //TODO throw exception instead
        return "";
    }

    static Action getActionFromCmd(String cmd, String arg) {
        switch (cmd) {
            case "sort": {
                if (arg != null && arg.matches("^desc[ ]*$")) {
                    return Action.SortDescending;
                }
                return Action.SortAscending;
            }
            case "add":
                return Action.Add;
            case "rm":
                return Action.Remove;
            case "mv":
                return Action.ReplaceItem;
            case "rmlst":
                return Action.RemoveList;
            case "mvlst":
                return Action.RenameList;
            case "new":
                return Action.CreateNewList;
            case "help":
                return Action.Help;
            case "helpinline":
                return Action.HelpInline;
            case "helpcmd":
                return Action.HelpCmd;
        }

        return Action.Cancel;
    }

    static String getCmd(Action act) {
        switch (act) {
            case Add:
                return "add";
            case Remove:
                return "rm";
            case RemoveList:
                return "rmlst";
            case RenameList:
                return "mvlst";
            case ReplaceItem:
                return "mv";
            case SortAscending:
                return "sort";
            case SortDescending:
                return "sort";
            case Help:
                return "help";
            case CreateNewList:
                return "new";
            case HelpCmd:
                return "helpcmd";
            case HelpInline:
                return "helpinline";
        }

        return "";
    }

    static Action getActionFromKeywoard(String name) {
        switch (name) {
            case "add":
                return Action.Add;
            case "rm":
                return Action.Remove;
            case "crs":
                return Action.CreateNewList;
            case "rml":
                return Action.RemoveList;
            case "rnl":
                return Action.RenameList;
            case "rpi":
                return Action.ReplaceItem;
            case "sa":
                return Action.SortAscending;
            case "sd":
                return Action.SortDescending;
            case "hlp":
                return Action.HelpInline;
            case "cnl":
                return Action.Cancel;
        }

        //TODO throw exception instead
        return Action.Add;
    }

    static String getName(Action action) {
        switch (action) {
            case Add:
                return "Add an item";
            case Remove:
                return "Remove an item";
            case CreateNewList:
                return "Create a new simple list";
            case RemoveList:
                return "Remove list";
            case RenameList:
                return "Rename a list";
            case ReplaceItem:
                return "Replace an existing item";
            case SortAscending:
                return "Sort ascending";
            case SortDescending:
                return "Sort descending";
            case Help:
                return "Help";
            case Cancel:
                return "Cancel";
            case HelpInline:
                return "Help";
            case HelpCmd:
                return "Help";
        }

        //TODO throw exception instead
        return "";
    }
}
