import org.bson.BsonArray;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by m0x35 on 4/1/17.
 */

public class List {
    public static enum SortingOrder {
        SortAscending,
        SortDescending
    }

    public List(String title, java.util.List<String> content) {
        mTitle = title;
        mContent = content;
    }
    public List(String title) {
        this(title, new ArrayList<>());
    }

    public String title() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String toMessage() {
        String msg = "*" + mTitle + "*\n";

        if (!mContent.isEmpty()) {
            for (int i = 0; i < mContent.size(); i++)
                msg += String.format("\n%d. %s", i + 1, mContent.get(i));
        } else {
            msg += "\n_This list is empty._";
        }

        return msg;
    }

    public void move(int indexFrom, int indexTo) {
        mContent.add(indexTo, mContent.get(indexFrom));

        if (indexFrom > indexTo)
            indexFrom++;

        mContent.remove(indexFrom);
    }

    public void replace(int index, String value) {
        mContent.set(index, value);
    }

    public void remove(int index) {
        mContent.remove(index);
    }

    public void insert(int index, String value) {
        mContent.add(index, value);
    }

    public void insert(String value) {
        mContent.add(value);
    }

    public void sort(SortingOrder order) {
        switch (order) {
            case SortAscending:
                mContent.sort((String sl, String sr) -> sl.compareTo(sr));
                break;
            case SortDescending:
                mContent.sort((String sl, String sr) -> sr.compareTo(sl));
                break;
        }
    }

    public Document toBsonDoc() {
        return new Document("title", mTitle)
                .append("content", new BsonArray(mContent.stream().map((String s) -> new BsonString(s)).collect(Collectors.toList())));
    }

    public static List fromBsonDoc(Document doc) {
        java.util.List<String> content = (java.util.List)doc.get("content");

        if (content == null)
            content = new ArrayList<>();

        String title = doc.getString("title");

        return new List(title, content);
    }

    private String mTitle;
    private final java.util.List<String> mContent;
}
