import java.util.Optional;

/**
 * Created by m0x35 on 4/19/17.
 */
interface ActionSessionStorage {
    void store(ActionSession sess);
    void remove(ActionSession sess);
    Optional<ActionSession> get(String id);
}
