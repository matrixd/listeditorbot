import java.util.Optional;

/**
 * Created by m0x35 on 4/20/17.
 */
interface ListStorage {
    void store(String chatId, List list);
    Optional<List> get(String chatId);
    void remove(String chatId);
}
