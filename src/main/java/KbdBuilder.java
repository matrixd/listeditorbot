import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by m0x35 on 4/20/17.
 */
public class KbdBuilder {
    KbdBuilder(String sessId) {
        mSessId = sessId;
    }

    public InlineKeyboardMarkup build() {
        ArrayList<List<InlineKeyboardButton>> rows = new ArrayList<>();

        rows.add(Arrays.asList(createButton(BotActions.Action.CreateNewList)));
        rows.add(Arrays.asList(createButton(BotActions.Action.Add),
                createButton(BotActions.Action.Remove)));
        rows.add(Arrays.asList(createButton(BotActions.Action.SortAscending),
                createButton(BotActions.Action.SortDescending)));
        rows.add(Arrays.asList(createButton(BotActions.Action.RenameList)));
        rows.add(Arrays.asList(createButton(BotActions.Action.RemoveList)));
        rows.add(Arrays.asList(createButton(BotActions.Action.HelpInline)));
        rows.add(Arrays.asList(createButton(BotActions.Action.Cancel)));

        return new InlineKeyboardMarkup()
                .setKeyboard(rows);
    }

    InlineKeyboardButton createButton(BotActions.Action action) {
        return new InlineKeyboardButton()
                .setCallbackData(BotActions.getKeywoard(action) + ":" + mSessId)
                .setText(BotActions.getName(action));
    }

    private String mSessId;
}
