import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.api.objects.inlinequery.inputmessagecontent.InputMessageContent;
import org.telegram.telegrambots.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.api.objects.inlinequery.result.InlineQueryResultLocation;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by m0x35 on 4/20/17.
 */
public class ListEditorBot extends TelegramLongPollingBot {
    ListEditorBot(ActionSessionStorage sessionStorage, ListStorage listStorage) {
        super();
        mSessionStorage = sessionStorage;
        mListStorage = listStorage;
        mActionExecuter = new ActionExecuter(mListStorage);
    }

    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            handleMessage(update.getMessage());
        } else if (update.hasInlineQuery()) {
            handleInlineQuery(update.getInlineQuery());
        } else if (update.hasCallbackQuery()) {
            handleCallback(update.getCallbackQuery());
        } else {
            System.out.println("unhandled: " + update.toString());
        }

    }

    void handleCallback(CallbackQuery query) {
        String data = query.getData();
        String cmd = data.substring(0, data.indexOf(':'));
        String sessionId = data.substring(data.indexOf(':') + 1);

        Optional<ActionSession> sess = mSessionStorage.get(sessionId);

        AnswerCallbackQuery cbAnswer = new AnswerCallbackQuery()
                .setCallbackQueryId(query.getId());

        EditMessageText edit = new EditMessageText()
                .setInlineMessageId(query.getInlineMessageId())
                .setParseMode("Markdown");


        if (sess.isPresent()) {

            if (!sess.get().checkUser(query.getFrom().getId()))
            {
                cbAnswer.setText("You are not allowed to perform this action.");
            } else {

                BotActions.Action action = BotActions.getActionFromKeywoard(cmd);

                if (action == BotActions.Action.Cancel) {
                    edit.setText("_Cancelled_");
                } else {
                    edit.setText(mActionExecuter.execute(action, query.getChatInstance(),
                            sess.get().getUserInput()));
                }
            }
        } else {
            final String text = "Seems your session has expired. Please try again.";
            cbAnswer.setText(text);
            edit.setText(text);
        }

        try {
            sendApiMethod(cbAnswer);
            if (edit.getText() != null && !edit.getText().isEmpty())
                sendApiMethod(edit);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getBotUsername() {
        return "ListEditorBot";
    }

    public String getBotToken() {
        return Token.TOKEN;
    }

    public void onClosing() {

    }

    void handleMessage(Message msg) {
        String txt = msg.getText();
        if (txt == null || txt.isEmpty())
            return;

        final Pattern re = Pattern.compile(
                "^/(?<cmd>[a-z]+)@?(?<name>\\S*)\\s*(?<args>.*)",
                Pattern.DOTALL);

        Matcher m = re.matcher(txt);
        if (!m.matches())
            return;

        String name = m.group("name");
        if (!name.isEmpty() && !name.equals(getBotUsername())) {
            return;
        }

        handleCommand(msg, m.group("cmd"), m.group("args"));
    }

    void handleCommand(Message inputMsg, String cmd, String arg) {
        BotActions.Action action = BotActions.getActionFromCmd(cmd, arg);

        if (action == BotActions.Action.Cancel)
            return;

        if (arg == null)
            arg = "";

        String content = mActionExecuter.execute(
                action,
                String.valueOf(inputMsg.getChatId()),
                arg
        );

        SendMessage outMsg = new SendMessage()
                .setChatId(inputMsg.getChatId())
                .setText(content)
                .setParseMode("Markdown");
        try {
            sendMessage(outMsg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    void handleInlineQuery(InlineQuery query) {
        InputMessageContent msg = new InputTextMessageContent()
                .setMessageText("Hi, " + query.getFrom().getFirstName() + ". Please, select an action you want to perform on the previously entered text.");


        final ActionSession actionSession = new ActionSession(query.getFrom().getId());
        actionSession.setUserInput(query.getQuery());

        final String title = "Enter a text & click here";
        InlineQueryResultLocation option = fakeLocation(msg, title, actionSession);

        AnswerInlineQuery answer = new AnswerInlineQuery()
                .setPersonal(true)
                .setInlineQueryId(query.getId())
                .setCacheTime(1)
                .setResults(option);

        mSessionStorage.store(actionSession);

        try {
            sendApiMethod(answer);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    InlineQueryResultLocation fakeLocation(InputMessageContent msg, String title, ActionSession sess) {
        final Float latLongVal = new Float(0.0);
        KbdBuilder builder = new KbdBuilder(sess.getId());
        return new InlineQueryResultLocation()
                    .setId(sess.getId())
                    .setTitle(title)
                    .setLatitude(latLongVal)
                    .setLongitude(latLongVal)
                    .setInputMessageContent(msg)
                    .setReplyMarkup(builder.build());
    }

    private final ActionSessionStorage mSessionStorage;
    private final ListStorage mListStorage;
    private final ActionExecuter mActionExecuter;
}
