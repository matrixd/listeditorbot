import java.util.Optional;

/**
 * Created by m0x35 on 4/23/17.
 */
public class ActionExecuter {

    public ActionExecuter(ListStorage mStorage) {
        this.mStorage = mStorage;
    }

    /**
     *
     * @param action action except help and cancel
     * @param listId
     * @return resulting message
     */
    String execute(BotActions.Action action, String listId, String userInput) {
        if (action == BotActions.Action.CreateNewList) {
            String items[] = split(userInput);
            List lst = new List(items[0]);
            for (int i = 1; i < items.length; i++) {
                lst.insert(items[i]);
            }
            mStorage.store(listId, lst);
            return lst.toMessage();
        } else if (action == BotActions.Action.RemoveList) {
            mStorage.remove(listId);
            return "List successfully removed.";
        } else if (action == BotActions.Action.Help) {
            return Help.getHelloWord();
        } else if (action == BotActions.Action.HelpCmd) {
            return Help.getCmdHelp();
        } else if (action == BotActions.Action.HelpInline) {
            return Help.getInlineHelp();
        }

        Optional<List> optional = mStorage.get(listId);

        if (!optional.isPresent()) {
            return "You should create a list first";
        }

        List lst = optional.get();

        switch (action) {
            case RenameList:
                lst.setTitle(userInput);
                break;

            case SortDescending:
                lst.sort(List.SortingOrder.SortDescending);
                break;

            case SortAscending:
                lst.sort(List.SortingOrder.SortAscending);
                break;

            case ReplaceItem: {
                String x = replaceItem(userInput, lst);
                if (x != null) return x;
                break;
            }

            case Add: {
                String items[] = split(userInput);
                for (String s : items)
                    lst.insert(s);
                break;
            }

            case Remove: {
                String x = remove(userInput, lst);
                if (x != null) return x;
                break;
            }
        }

        mStorage.store(listId, lst);
        return lst.toMessage();
    }

    private String replaceItem(String userInput, List lst) {
        String input[] = split(userInput);
        if (input.length % 2 > 0) {
            return "Invalid input. You should enter itemIndex;NewItem;itemIndex...";
        }

        for (int i = 0; i < input.length; i++) {
            try {
                lst.replace(Integer.valueOf(input[i]) - 1, input[++i]);
            } catch (IndexOutOfBoundsException e) {
                return "Invalid index(es) to replace (out of bounds).";
            } catch (NumberFormatException e) {
                return "Invalid index(es) to replace.";
            }
        }
        return null;
    }

    private String remove(String userInput, List lst) {
        String items[] = split(userInput);
        try {
            for (String s : items)
                lst.remove(Integer.valueOf(s));
        } catch (NumberFormatException e) {
            //TODO separate error messages from this class
            return "Invalid index(es) to remove.";
        } catch (IndexOutOfBoundsException e) {
            return "Invalid index(es) to remove (out of bounds).";
        }
        return null;
    }


    String[] trim(String[] lst) {
        for (int i = 0; i < lst.length; i++) {
            lst[i] = lst[i].trim();
        }

        return lst;
    }

    String[] split(String userInput) {
        String lst[] = userInput.split(";");
        if (lst.length < 2) {
            lst = userInput.split("\n");
        }

        return trim(lst);
    }
    private final ListStorage mStorage;
}
