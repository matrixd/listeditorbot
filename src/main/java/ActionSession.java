import java.util.Random;
import java.util.UUID;

/**
 * Created by m0x35 on 4/19/17.
 */
public class ActionSession {
    ActionSession(long userId) {
        mUserId = userId;
        Random r = new Random();
        mId = new UUID(r.nextLong(), r.nextLong()).toString();
    }

    ActionSession(String id, long userId, String userInput) {
        mUserId = userId;
        mId = id;
        mUserInput = userInput;
    }

    boolean checkUser(long userId) {
        return userId == mUserId;
    }

    long userId() {
        return mUserId;
    }

    public String getUserInput() {
        return mUserInput;
    }

    public void setUserInput(String userInput) {
        this.mUserInput = userInput;
    }

    public int getChoosedOption() {
        return mChoosedOption;
    }

    public void setChoosedOption(int choosedOption) {
        this.mChoosedOption = choosedOption;
    }

    public String getId() {
        return mId;
    }

    private final long mUserId;
    private String mUserInput;
    private int mChoosedOption;

    private final String mId;
}
